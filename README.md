INTRODUCTION
============

Views responsive columns displays views results in a flexible number of columns
based on current screen width.

REQUIREMENTS
============

 * Views module

INSTALLATION
============

Install as you would normally install a contributed Drupal module. For more
information, see [Installing contributed modules](https://www.drupal.org/documentation/install/modules-themes/modules-8).

CONFIGURATION
=============

 * Create a new view or edit an existing one.
 * Select "Responsive Columns" as the view format.
 * Define column count and break point width, or use the provided defaults.